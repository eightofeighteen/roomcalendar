﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomCalendar.Models
{
    public class CalendarEntry
    {
        public string subject { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        //public DateTime startDT { get; set; }
        //public DateTime endDT { get; set; }
        public double durationInMins()
        {
            return (end - start).TotalMinutes;
        }
        public string text
        {
            get { return subject; }
        }
        public int id { get; set; }
    }
}