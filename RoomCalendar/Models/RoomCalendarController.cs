﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Exchange.WebServices.Data;

namespace RoomCalendar.Models
{
    public class RoomCalendarController : Controller
    {
        //
        // GET: /RoomCalendar/
        public ActionResult Index()
        {
            return View();
        }

        private List<CalendarEntry> getCalendarEntries(string mailbox, string username, string password, string server = "")
        {
            List<CalendarEntry> calEntries = new List<CalendarEntry>();
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            service.Credentials = new WebCredentials(username, password);
            //service.AutodiscoverUrl("roommonitor@illovo.co.za");
            service.Url = new Uri("https://exchhub.za.illovo.net/EWS/Exchange.asmx");

            // Limit the result set to 10 items.
            ItemView view = new ItemView(10);

            view.PropertySet = new PropertySet(ItemSchema.Subject,
                                               ItemSchema.DateTimeReceived,
                                               EmailMessageSchema.IsRead);

            // Item searches do not support deep traversal.
            view.Traversal = ItemTraversal.Shallow;

            // Define the sort order.
            view.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Descending);

            try
            {
                // Call FindItems to find matching calendar items. 
                // The FindItems parameters must denote the mailbox owner,
                // mailbox, and Calendar folder.
                // This method call results in a FindItem call to EWS.
                FindItemsResults<Item> results = service.FindItems(
                new FolderId(WellKnownFolderName.Calendar,
                    mailbox),
                    (SearchFilter)null,
                    view);

                var propertySet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject,
            AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.Location);

                service.LoadPropertiesForItems(results.Items, propertySet);

                int c = 0;
                foreach (Item item in results.Items)
                {
                    c++;
                    CalendarEntry entry = new CalendarEntry();
                    entry.subject = item.Subject.ToString();
                    entry.start = ((Appointment)item).Start;
                    entry.end = ((Appointment)item).End;
                    //entry.start = a.Start.ToString();
                    //entry.end = a.End.ToString();
                    entry.id = c;
                    calEntries.Add(entry); 
                    /*Console.WriteLine("Subject: {0}", item.Subject);
                    Console.WriteLine(((Appointment)item).Start.ToString());
                    Console.WriteLine(((Appointment)item).End.ToString());

                    Console.WriteLine();*/
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception while             enumerating results: {0}", ex.Message);
            }

            /*try
            {
                
                // Connect to Exchange Web Services as user1 at contoso.com.
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
                service.Credentials = new WebCredentials(username, password);
                service.AutodiscoverUrl(mailbox);
                


                // Write confirmation message to console window.
                //Console.WriteLine("Message sent!");
                // Initialize values for the start and end times, and the number of appointments to retrieve.
                DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                //startDate.AddHours(-startDate.Hour);
                //startDate.AddMinutes(-startDate.Minute);
                //startDate.AddSeconds(-startDate.Second);
                DateTime endDate = startDate.AddDays(30);
                const int NUM_APPTS = 100;

                // Initialize the calendar folder object with only the folder ID. 
                CalendarFolder calendar = CalendarFolder.Bind(service, WellKnownFolderName.Calendar, new PropertySet());

                // Set the start and end time and number of appointments to retrieve.
                CalendarView cView = new CalendarView(startDate, endDate, NUM_APPTS);

                // Limit the properties returned to the appointment's subject, start time, and end time.
                cView.PropertySet = new PropertySet(AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);

                // Retrieve a collection of appointments by using the calendar view.
                FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);

                //Console.WriteLine("\nThe first " + NUM_APPTS + " appointments on your calendar from " + startDate.Date.ToShortDateString() +
                //                  " to " + endDate.Date.ToShortDateString() + " are: \n");
                int c = 0;
                foreach (Appointment a in appointments)
                {
                    c++;
                    CalendarEntry entry = new CalendarEntry();
                    entry.subject = a.Subject.ToString();
                    entry.start = a.Start;
                    entry.end = a.End;
                    //entry.start = a.Start.ToString();
                    //entry.end = a.End.ToString();
                    entry.id = c;
                    calEntries.Add(entry);
                }
                
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message.ToString();
            }*/

            return calEntries;
        }

        public ActionResult Events(string room, string mailbox)
        {
            
            string username = "za\\roommonitor";
            string password = "Illovo555";
            //string mailbox = "room.hobaobab@illovo.co.za";
            mailbox = HttpUtility.HtmlDecode(mailbox);
            List<CalendarEntry> calEntries = getCalendarEntries(mailbox, username, password);
            ViewBag.RoomName = room;
            ViewBag.Mailbox = mailbox;
            return Json(new JsonResult { Data = calEntries }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Calendar(string room, string mailbox)
        {
            //List<CalendarEntry> calEntries = getCalendarEntries(mailbox, username, password);
            List<CalendarEntry> calEntries = new List<CalendarEntry>();
            ViewBag.RoomName = room;
            mailbox = HttpUtility.HtmlDecode(mailbox);
            ViewBag.Mailbox = mailbox;
            return View(calEntries);
        }

        public string ErrorMessage(string error)
        {
            return error;
        }
	}
}